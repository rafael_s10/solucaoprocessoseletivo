<!DOCTYPE html>
<%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1"pageEncoding="ISO-8859-1"%> --%>
<!-- <!DOCTYPE html > -->
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<!-- http://getbootstrap.com/
http://getbootstrap.com/components/
https://code.jquery.com/jquery-1.10.2.min.js -->

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Hello Web World</title>
		
		<link href="assets/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<script src="assets/js/jquery-1.10.2.min.js" type="text/javascript"></script>
		
		<script type="text/javascript">
		$(document).ready(function(){			
			/*definindo campos obrigatorios */
			$('#nome').attr("required", "required");
			$('#email').attr("required", "required");
			
			
			$("#enviar").click(function(){
				executar( "enviar" );
			});
			
			$("#limpar").click(function(){
				executarComSubmit( "limpar" );
			});
		});
		
		function executar( nomeMetodo ){
		    document.getElementById("form_candidato").method.value = nomeMetodo;
			}
		
		function executarComSubmit( nomeMetodo ){
		    document.getElementById("form_candidato").method.value = nomeMetodo;
		    document.getElementById("form_candidato").submit();
		}
		
		function checkRegexp(o, regexp, n) {
		    if (!(regexp.test(o))) {
		     return false;
		    } else {
		     return true;
		    }
		   }

		   function validarEmail(obj) {
		    if (obj.value.length > 0) {
		     if (checkRegexp(
		       obj.value,
		       /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i,
		       "Ex: adm@gruposrc.com.br") == false) {
		      obj.focus();
		      obj.style.background = '#FFCC66';
		     } else {
		      obj.style.background = 'white';
		     }
		    } else {
		     obj.style.background = 'white';
		    }
		   }
			
			
		</script>
		
		
	</head>
	<body>
		
		<html:form styleId="form_candidato" action="/candidatoAction.do" method="post">
		<html:hidden property="method" value="empty"/>
		
		<div class="container">
			
			<div class="panel panel-group panel-primary">
				
				<!-- Esse panel � o cabe�alho da p�gina -->
				<div class="panel-heading">
					<i class="glyphicon glyphicon-user"></i>
					Candidato
				</div>
				
				
				<!-- Esse � o painel do corpo da minha janela -->
				<div class="panel-body">
					
					<!-- Linha -->
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bg-danger">
							<form>
							
								<div class="row">
										
									<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-5">
										<label for="nome" class="control-label" >Nome</label>
										<html:text  styleId="nome" styleClass="form-control obrigatorio"  property="nome" name = "candidatoForm"/>
									</div>
									
									<div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-5">
										<label for="email" class="control-label">E-mail</label>
										<html:text  styleId="email" styleClass="form-control obrigatorio"  property="email" name = "candidatoForm"  onblur="validarEmail(this);" onkeyup="validarEmail(this);"/>
									</div>
									
									
									<div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-2">
           								<label>HTML</label>
           								<html:select styleClass="form-control" styleId="html" property="html" name="candidatoForm">
            								<html:option value="0">0</html:option>
           									<html:option value="1">1</html:option>
            								<html:option value="2">2</html:option>
            								<html:option value="3">3</html:option>
            								<html:option value="4">4</html:option>
            								<html:option value="5">5</html:option>
            								<html:option value="6">6</html:option>
            								<html:option value="7">7</html:option>
            								<html:option value="8">8</html:option>
            								<html:option value="9">9</html:option>
            								<html:option value="10">10</html:option>
           								</html:select>
         							 </div>
									
									 <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-2">
           								<label>CSS</label>
           								<html:select styleClass="form-control" styleId="css" property="css" name="candidatoForm">
            								<html:option value="0">0</html:option>
           									<html:option value="1">1</html:option>
            								<html:option value="2">2</html:option>
            								<html:option value="3">3</html:option>
            								<html:option value="4">4</html:option>
            								<html:option value="5">5</html:option>
            								<html:option value="6">6</html:option>
            								<html:option value="7">7</html:option>
            								<html:option value="8">8</html:option>
            								<html:option value="9">9</html:option>
            								<html:option value="10">10</html:option>
           								</html:select>
         							 </div>
									
									<div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-2">
           								<label>Javascript</label>
           								<html:select styleClass="form-control" styleId="javascript" property="javascript" name="candidatoForm">
            								<html:option value="0">0</html:option>
           									<html:option value="1">1</html:option>
            								<html:option value="2">2</html:option>
            								<html:option value="3">3</html:option>
            								<html:option value="4">4</html:option>
            								<html:option value="5">5</html:option>
            								<html:option value="6">6</html:option>
            								<html:option value="7">7</html:option>
            								<html:option value="8">8</html:option>
            								<html:option value="9">9</html:option>
            								<html:option value="10">10</html:option>
           								</html:select>
         							 </div>
									
									<div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-2">
           								<label>Python</label>
           								<html:select styleClass="form-control" styleId="python" property="python" name="candidatoForm">
            								<html:option value="0">0</html:option>
           									<html:option value="1">1</html:option>
            								<html:option value="2">2</html:option>
            								<html:option value="3">3</html:option>
            								<html:option value="4">4</html:option>
            								<html:option value="5">5</html:option>
            								<html:option value="6">6</html:option>
            								<html:option value="7">7</html:option>
            								<html:option value="8">8</html:option>
            								<html:option value="9">9</html:option>
            								<html:option value="10">10</html:option>
           								</html:select>
         							 </div>
									
									<div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-2">
           								<label>Django</label>
           								<html:select styleClass="form-control" styleId="django" property="django" name="candidatoForm">
            								<html:option value="0">0</html:option>
           									<html:option value="1">1</html:option>
            								<html:option value="2">2</html:option>
            								<html:option value="3">3</html:option>
            								<html:option value="4">4</html:option>
            								<html:option value="5">5</html:option>
            								<html:option value="6">6</html:option>
            								<html:option value="7">7</html:option>
            								<html:option value="8">8</html:option>
            								<html:option value="9">9</html:option>
            								<html:option value="10">10</html:option>
           								</html:select>
         							 </div>	
									
									<div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-2">
           								<label>Desenv. IOS</label>
           								<html:select styleClass="form-control" styleId="desenvolvimentoIos" property="desenvolvimentoIos" name="candidatoForm">
            								<html:option value="0">0</html:option>
           									<html:option value="1">1</html:option>
            								<html:option value="2">2</html:option>
            								<html:option value="3">3</html:option>
            								<html:option value="4">4</html:option>
            								<html:option value="5">5</html:option>
            								<html:option value="6">6</html:option>
            								<html:option value="7">7</html:option>
            								<html:option value="8">8</html:option>
            								<html:option value="9">9</html:option>
            								<html:option value="10">10</html:option>
           								</html:select>
         							 </div>	
									
									<div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-2">
           								<label>Desenv. Android</label>
           								<html:select styleClass="form-control" styleId="desenvolvimentoAndroid" property="desenvolvimentoAndroid" name="candidatoForm">
            								<html:option value="0">0</html:option>
           									<html:option value="1">1</html:option>
            								<html:option value="2">2</html:option>
            								<html:option value="3">3</html:option>
            								<html:option value="4">4</html:option>
            								<html:option value="5">5</html:option>
            								<html:option value="6">6</html:option>
            								<html:option value="7">7</html:option>
            								<html:option value="8">8</html:option>
            								<html:option value="9">9</html:option>
            								<html:option value="10">10</html:option>
           								</html:select>
         							 </div>	
												
								</div>
								
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										
										<button type="submit" id="enviar" class="btn btn-success">Enviar</button>										
										<button type="button" id="limpar" class="btn btn-warning">Limpar</button>
										
									</div>
								</div>
								</form>						
						</div>
					</div>
				</div>

				<!-- Esse panel � o Rodape da p�gina -->
				<div class="footer">
					Meus Pedidos &copy;
				</div>
			</div>
		
		</div>
		</html:form>		
	</body>	
</html>