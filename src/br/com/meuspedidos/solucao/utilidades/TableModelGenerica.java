package br.com.meuspedidos.solucao.utilidades;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

public final class TableModelGenerica extends AbstractTableModel{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4709514314214817940L;

	/**
	 * Atributo respons�vel por armazenar os dados referentes ao
	 * cabe�alho da JTable. Ex: Colunas (String[] cabecalho) A B C D
	 */
	private String[ ] cabecalho;

	/**
	 * Atributo respons�vel por armazenar os dados referentes as
	 * dados da JTable. Ex: dados (ArrayList) 1 2 3.
	 * Agora juntando o ArrayList(dados) com ArrayList<String>(colunas) -->
	 * ArrayList<ArrayList<String>>: A B C D 1 2 3
	 */
	private ArrayList< ArrayList< String > > tabela = new ArrayList< ArrayList< String > >();

	public TableModelGenerica( String[ ] cabecalho ){
		this.cabecalho = cabecalho;
	}

	/**
	 * M�todo respons�vel por retornar o valor do atributo cabecalho.
	 * 
	 * @return String[] cabecalho - cabecalho a ser retornado(angel).
	 */
	public String[ ] getCabecalho() {
		if ( cabecalho == null ) {
			cabecalho = new String[ ] { "?????????" };
		}
		return cabecalho;
	}

	/**
	 * Retorna para a JTable a quantidade de dados que ela tem.
	 * Polimorfico
	 * 
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return tabela.size();
	}

	/**
	 * Retorna para a JTable a quantidade de colunas que ela tem.
	 * Polimorfico
	 * 
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return cabecalho.length;
	}

	/**
	 * M�todo padr�o utilizado pela JTable para a nomea��o das colunas(cabe�alho).
	 * Polimorfico
	 * 
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	@Override
	public String getColumnName( int coluna ) {
		return getCabecalho()[ coluna ];
	}

	/**
	 * M�todo padr�o utilizado para recuperar dados de uma determinada celula.
	 * 
	 * @param int linha - Linha onde est� o valor.
	 * @param int coluna - Coluna onde est� o valor.
	 * @return String - Dado localizado na celula em questao.
	 *         Polimorfico
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt( int linha, int coluna ) {
		// Pegando os dados contidos numa celula especifica.
		String valorCelula = tabela.get( linha ).get( coluna );
		return valorCelula;
	}

	/**
	 * M�todo respons�vel por adicionar dados na tabela.
	 * 
	 * @param ArrayList<String> linha.
	 * 
	 * @author Gabriel Damiani Carvalheiro <gabriel.carvalheiro@gmail.com.br>
	 * @since 06/10/2014 15:36:40
	 * @version 1.0
	 */
	public void addLinha( ArrayList< String > linha ) {
		// Adicionando uma linha na tabela
		tabela.add( linha );

		// Confirmando a inser��o da linha em quest�o
		fireTableRowsInserted( getRowCount(), getRowCount() );
	}

	/**
	 * M�todo respons�vel por recuperar uma linha
	 * da tabela.
	 * 
	 * @param int linha - linha informada
	 * @return = ArrayList<String> linha selecionada
	 * 
	 * @author Gabriel Damiani Carvalheiro <gabriel.carvalheiro@gmail.com.br>
	 * @since 06/10/2014 16:11:14
	 * @version 1.0
	 */
	public ArrayList< String > getLinha( int linha ) {
		return tabela.get( linha );
	}

	/**
	 * M�todo respons�vel por remover as dados da tabela.
	 */
	public void limparTabela() {
		if ( getRowCount() > 0 ) {
			fireTableRowsDeleted( 0, getRowCount() );

			tabela.clear();
		}
	}
}
