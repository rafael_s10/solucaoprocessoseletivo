package br.com.meuspedidos.solucao.utilidades;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public abstract class SendMail{
	public static void enviar( String assunto, String conteudo, String emailTo ) {
		Properties props = new Properties();
		props.put( "mail.transport.protocol", "smtp" ); //define protocolo de envio como SMTP
		  props.put( "mail.smtp.starttls.enable", "true" );
		  props.put( "mail.smtp.host", "smtp.gmail.com" ); //server SMTP do GMAIL
		  props.put( "mail.smtp.auth", "true" ); //ativa br.com.srcsoftware.autenticacao
		  props.put( "mail.smtp.user", "rafaelguimaraessantos3@gmail.com" ); //usuario ou seja, a conta que esta enviando o email (tem que ser do GMAIL)
		  props.put( "mail.debug", "true" );
		  props.put( "mail.smtp.port", "587" ); //porta
		  props.put( "mail.smtp.socketFactory.port", "587" ); //mesma porta para o socket
		  //props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		  props.put( "mail.smtp.socketFactory.fallback", "false" );
		
		  Session s = Session.getDefaultInstance( props, new javax.mail.Authenticator(){
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication( "rafaelguimaraessantos3@gmail.com", "rafinha101" );
				}
			} );
		  s.setDebug( true ); //Habilita o LOG das a��es executadas durante o envio do email
		  
		/*props.put( "mail.smtp.host", "smtp.gmail.com" );
		props.put( "mail.smtp.socketFactory.port", "465" );
		props.put( "mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory" );
		props.put( "mail.smtp.auth", "true" );
		props.put( "mail.smtp.port", "465" );*/

		/*Session session = Session.getDefaultInstance( props, new javax.mail.Authenticator(){
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication( "rafaelguimaraessantos3@gmail.com", "rafinha101" );
			}
		} );*/

		try {

			Message message = new MimeMessage( s );
			message.setFrom( new InternetAddress( "rafaelguimaraessantos3@gmail.com" ) );
			message.setRecipients( Message.RecipientType.TO, InternetAddress.parse( emailTo ) );
			message.setSubject( assunto );
			message.setText( conteudo );

			Transport.send( message );

			System.out.println( "Done" );

		} catch ( MessagingException e ) {
			throw new RuntimeException( e );
		}
	}
}
