package br.com.meuspedidos.solucao.abstracts;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * 
 * Classe que representa a m�e de todos os POs do sistema.
 * 
 * @author Lucas Pedro Bermejo <lucas_bermejo@hotmail.com>
 * @since 09/01/2015 09:09:32
 * @version 1.0
 */
@MappedSuperclass
public abstract class AbstractPO{

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "codigo" )
	private Long codigo;

	public AbstractPO(){}

	public AbstractPO( Long codigo ){
		setCodigo( codigo );
	}

	/**
	 * M�todo respons�vel por retornar o valor do atributo codigo.
	 * 
	 * @return Long codigo - codigo a ser retornado(a).
	 */
	public final Long getCodigo() {
		return codigo;
	}

	/**
	 * M�todo respons�vel por atribuir o valor ao atributo codigo.
	 * 
	 * @param Long
	 *        codigo - codigo a ser atribuido(a).
	 */
	public final void setCodigo( Long codigo ) {
		this.codigo = codigo;
	}

	/**
	 * Polimorfico
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( codigo == null ) ? 0 : codigo.hashCode() );
		return result;
	}

	/**
	 * Polimorfico
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( !( obj instanceof AbstractPO ) ) {
			return false;
		}
		AbstractPO other = (AbstractPO) obj;
		if ( codigo == null ) {
			if ( other.codigo != null ) {
				return false;
			}
		} else if ( !codigo.equals( other.codigo ) ) {
			return false;
		}
		return true;
	}

	public abstract String toString();
}
