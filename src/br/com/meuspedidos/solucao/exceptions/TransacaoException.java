package br.com.meuspedidos.solucao.exceptions;

import java.sql.SQLException;

/**
 * Classe que representa o erro ocorrido no ato da
 * confirma��o dos processos realizados na conex�o.
 * 
 * @author Rafael Guimaraes
 * @since 14/10/2015
 * @version 1.0
 */
public final class TransacaoException extends SQLException{

	public TransacaoException( String message ){
		super( message );
	}

	public TransacaoException( String message, Throwable cause ){
		super( message, cause );
	}
}
