package br.com.meuspedidos.solucao.candidato.model;

import br.com.meuspedidos.solucao.exceptions.ApplicationException;
import br.com.meuspedidos.solucao.utilidades.SendMail;

public final class CandidatoSERVICE{

	public CandidatoSERVICE(){

	}

	public void enviar( CandidatoPO po ) throws ApplicationException {
		try {
			if( (po.getCss() >= 7 && po.getCss() <= 10) && (po.getHtml() >= 7 && po.getHtml() <=10 ) && (po.getJavascript() >= 7 && po.getJavascript() <=10)  ){
				System.out.println("Front-end");
				String assunto = "Obrigado por se candidatar";
				String conteudo = "Obrigado por se candidatar, assim que tivermos uma vaga disponível para programador Front-End entraremos em contato.";
				
				SendMail.enviar( assunto, conteudo, po.getEmail() );
			}
			
			if( (po.getPython() >= 7 && po.getPython() <= 10) && (po.getDjango() >= 7 && po.getDjango() <=10 )  ){
				System.out.println("back-end");
				String assunto = "Obrigado por se candidatar";
				String conteudo = "Obrigado por se candidatar, assim que tivermos uma vaga disponível para programador Back-End entraremos em contato.";
				
				SendMail.enviar( assunto, conteudo, po.getEmail() );
			}
			
			if( (po.getDesenvolvimentoAndroid() >= 7 && po.getDesenvolvimentoAndroid() <= 10) && (po.getDesenvolvimentoIos() >= 7 && po.getDesenvolvimentoIos() <=10 )  ){
				System.out.println("mobile");
				String assunto = "Obrigado por se candidatar";
				String conteudo = "Obrigado por se candidatar, assim que tivermos uma vaga disponível para programador Mobile entraremos em contato.";
				
				SendMail.enviar( assunto, conteudo, po.getEmail() );
			}
			
			if( !((po.getDesenvolvimentoAndroid() >= 7 && po.getDesenvolvimentoAndroid() <= 10) && (po.getDesenvolvimentoIos() >= 7 && po.getDesenvolvimentoIos() <=10 ) || (po.getCss() >= 7 && po.getCss() <= 10) && (po.getHtml() >= 7 && po.getHtml() <=10 ) && (po.getJavascript() >= 7 && po.getJavascript() <=10) || (po.getPython() >= 7 && po.getPython() <= 10) && (po.getDjango() >= 7 && po.getDjango() <=10 ) ) ){
				System.out.println("Generico");
				String assunto = "Obrigado por se candidatar";
				String conteudo = "Obrigado por se candidatar, assim que tivermos uma vaga disponível para programador entraremos em contato.";
				
				SendMail.enviar( assunto, conteudo, po.getEmail() );
			}

			
		} catch ( Exception e ) {
			throw new ApplicationException( "Flha ao enviar formulario", e );
		}
	}

}
