package br.com.meuspedidos.solucao.candidato.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.meuspedidos.solucao.abstracts.AbstractPO;

@Entity
@Table( name = "candidatos", schema = "solucao" )
public class CandidatoPO extends AbstractPO{	
	/*@Id
	@Column( name = "codigo", nullable = false, length = 100 )
	private Long codigo;*/
	
	@Column( name = "nome", nullable = false, length = 100 )
	private String nome;

	@Column( name = "email", nullable = false, length = 100 )
	private String email;

	@Column( name = "html", nullable = false, length = 50 )
	private Byte html;

	@Column( name = "css", nullable = false, length = 100 )
	private Byte css;

	@Column( name = "javascript", nullable = false, length = 100 )
	private Byte javascript;

	@Column( name = "python", nullable = false, length = 100 )
	private Byte python;

	@Column( name = "django", nullable = false, length = 100 )
	private Byte django;

	@Column( name = "desenvolvimento_ios", nullable = false, length = 100 )
	private Byte desenvolvimentoIos;

	@Column( name = "desenvolvimento_android", nullable = false, length = 100 )
	private Byte desenvolvimentoAndroid;

	public CandidatoPO(){}

	public CandidatoPO( String nome, String email, Byte html, Byte css, Byte javascript, Byte python, Byte django, Byte desenvolvimentoIos, Byte desenvolvimentoAndroid ){
		setNome( nome );
		setEmail( email );
		setCss( css );
		setDesenvolvimentoAndroid( desenvolvimentoAndroid );
		setDesenvolvimentoIos( desenvolvimentoIos );
		setDjango( django );
		setHtml( html );
		setJavascript( javascript );
		setPython( python );

	}

	public String getNome() {
		return nome;
	}

	public void setNome( String nome ) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail( String email ) {
		this.email = email;
	}

	public Byte getHtml() {
		return html;
	}

	public void setHtml( Byte html ) {
		this.html = html;
	}

	public Byte getCss() {
		return css;
	}

	public void setCss( Byte css ) {
		this.css = css;
	}

	public Byte getJavascript() {
		return javascript;
	}

	public void setJavascript( Byte javascript ) {
		this.javascript = javascript;
	}

	public Byte getPython() {
		return python;
	}

	public void setPython( Byte python ) {
		this.python = python;
	}

	public Byte getDjango() {
		return django;
	}

	public void setDjango( Byte django ) {
		this.django = django;
	}

	public Byte getDesenvolvimentoIos() {
		return desenvolvimentoIos;
	}

	public void setDesenvolvimentoIos( Byte desenvolvimentoIos ) {
		this.desenvolvimentoIos = desenvolvimentoIos;
	}

	public Byte getDesenvolvimentoAndroid() {
		return desenvolvimentoAndroid;
	}

	public void setDesenvolvimentoAndroid( Byte desenvolvimentoAndroid ) {
		this.desenvolvimentoAndroid = desenvolvimentoAndroid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ( ( css == null ) ? 0 : css.hashCode() );
		result = prime * result + ( ( desenvolvimentoAndroid == null ) ? 0 : desenvolvimentoAndroid.hashCode() );
		result = prime * result + ( ( desenvolvimentoIos == null ) ? 0 : desenvolvimentoIos.hashCode() );
		result = prime * result + ( ( django == null ) ? 0 : django.hashCode() );
		result = prime * result + ( ( email == null ) ? 0 : email.hashCode() );
		result = prime * result + ( ( html == null ) ? 0 : html.hashCode() );
		result = prime * result + ( ( javascript == null ) ? 0 : javascript.hashCode() );
		result = prime * result + ( ( nome == null ) ? 0 : nome.hashCode() );
		result = prime * result + ( ( python == null ) ? 0 : python.hashCode() );
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj )
			return true;
		if ( !super.equals( obj ) )
			return false;
		if ( getClass() != obj.getClass() )
			return false;
		CandidatoPO other = (CandidatoPO) obj;
		if ( css == null ) {
			if ( other.css != null )
				return false;
		} else if ( !css.equals( other.css ) )
			return false;
		if ( desenvolvimentoAndroid == null ) {
			if ( other.desenvolvimentoAndroid != null )
				return false;
		} else if ( !desenvolvimentoAndroid.equals( other.desenvolvimentoAndroid ) )
			return false;
		if ( desenvolvimentoIos == null ) {
			if ( other.desenvolvimentoIos != null )
				return false;
		} else if ( !desenvolvimentoIos.equals( other.desenvolvimentoIos ) )
			return false;
		if ( django == null ) {
			if ( other.django != null )
				return false;
		} else if ( !django.equals( other.django ) )
			return false;
		if ( email == null ) {
			if ( other.email != null )
				return false;
		} else if ( !email.equals( other.email ) )
			return false;
		if ( html == null ) {
			if ( other.html != null )
				return false;
		} else if ( !html.equals( other.html ) )
			return false;
		if ( javascript == null ) {
			if ( other.javascript != null )
				return false;
		} else if ( !javascript.equals( other.javascript ) )
			return false;
		if ( nome == null ) {
			if ( other.nome != null )
				return false;
		} else if ( !nome.equals( other.nome ) )
			return false;
		if ( python == null ) {
			if ( other.python != null )
				return false;
		} else if ( !python.equals( other.python ) )
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CandidatoPO [getNome()=" + getNome() + ", getEmail()=" + getEmail() + ", getHtml()=" + getHtml() + ", getCss()=" + getCss() + ", getJavascript()=" + getJavascript() + ", getPython()=" + getPython() + ", getDjango()=" + getDjango() + ", getDesenvolvimentoIos()=" + getDesenvolvimentoIos() + ", getDesenvolvimentoAndroid()=" + getDesenvolvimentoAndroid() + ", hashCode()=" + hashCode() + "]";
	}

}
